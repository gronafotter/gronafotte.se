---
layout: default
---

# En Grön Folkrörelse för den vanliga människan
När jag sökte möjlighet att engagera mig i någon form av grön förening blev det klurigt. Jag är inte omställare, men odlar gärna. Jag är medlem i Naturskyddsföreningen men det är mest ett stöd, jag söker något att engagera mig i. Hittade flera föreningen för ungdomar, men jag är ingen ungdom. 

Jag upplevde att det finns ett tomrum att fylla; en grönt inriktad förening för vanliga personer och som ger möjlighet till engagemang. Och inget kommer att hända om ingen sätter fart. 

Detta är upprinnelsen till Gröna Fötter, ett arbetsnamn på ett initiativ för att bygga en grön folkrörelse. 

Gröna Fötter är ännu ingen förening. Det är en idé. Som jag hoppas skall bli en förening. 

Vill du vara med att forma Gröna Fötter eller vill veta mer om det händer något är du välkommen att kontakta oss på epost uppstart@gronafotter.se. 

Erik R

---
layout: post
title: Hej Världen!
tags: Uppstart
---
Hej Världen!

Jag mins inte riktigt när det började men sommaren 2019 var det ett riktigt skoskav - behovet av en folknära grön rörelse för vem som helst, för de som inte vill vara på pretentiösa. Under sommaren skissade jag på vad en sådan förening i så fall borde vara och bli. 

Det gick ända fram till Februari 2020 innan jag äntligen fixade till en hemsida och kände mig redo att böjra kommunicera kring idén med en hemsida. Den fick namnet Gröna Fötter och detta är dess första inlägg. 

Vill du vara med att forma en spännande resa eller vill du bara följa med på resan - tveka inte att kontakta oss (i skirvande stund: mig) på uppstart@gronafotter.se

// Erik R



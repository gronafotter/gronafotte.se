---
layout: page
title: Personlig Integritet
permalink: /gdpr/
order: 3
share: false
---

### Några ord om din personliga integritet på hemsidan. 
Hemsidan använder inga cookies, samlar inte in någon information om dig och är mycket sparsam med Java-Script.







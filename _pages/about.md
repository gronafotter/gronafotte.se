---
layout: page
title: Om Oss
permalink: /about/
order: 1
share: false
---

Vid förändringar eller vid nya sätt att arbeta kommer olika personer att förhålla sig olika mot det som håller på att ske. Vissa är entusiastiska och springer före, vissa är direkt motståndare medan de flesta är förhållandevis neutrala.

Gröna Fötter är (ännu så länge en idé om) en grön föreningen som vänder sig till den breda grupp av personer som inte riktigt känner sig så entusiastiska att man blir omställare eller aktivist i Green Peace. Ambitionen är att skapa en bred rörelse som man skall _vilja_ vara med i, som fungerar i vardagen och som samtidigt visar på det positiva i att vidga sitt perspektiv lite.

Föreningens rötter finns i de tre solidariteterna;
* Solidaritet med djur, natur och det ekologiska systemet
* Solidaritet med kommande generationer
* Solidaritet med världens alla människor.

Wikipedia beskriver solidaritet som "Solidaritet innebär att kollektiv ta ansvar för något, inbördes gemenskap, att utan egenintresse i egenskap av del av en grupp verka hänsynsfullt för denna grupps bästa."

Ambitionen med Gröna Fötter är att vidga perspektiv i människors vardag, inte att rädda världen, att visa på de positiva effekter som de tre solidariteterna kan ge på oss både som individer och som samhälle. Vi skall samtidigt uppmuntra till och finnas som stöd för var och ens individuella resa och utveckling.

Gröna Fötter har dessutom som ambition att samarbeta med andra föreningar och rörelser där dagens entusiaster finns tillsammans med kunskap och energi och på så sätt fungera som ett nätverk.

Föreningen är och kommer att förbli politiskt oberoende. 

